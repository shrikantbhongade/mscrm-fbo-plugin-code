﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F_B_O
{
    public class CreateOpp : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            if (context.Depth > 1)
            {
                return;
            }
            try
            {
                ////Create an entity object with the Id referencing the entity current targeted for Update
                //Entity entity = new Entity(context.PrimaryEntityName);
                //entity.Id = context.PrimaryEntityId;
                //entity["fbo_fbo_solicitation_number"] = "A string value";
                ////Replace the targeted entity object with the newly created entity object that references the common Id
                //context.InputParameters["Target"] = entity;
                //service.Update(entity);
            }
            catch (Exception ex)
            {
                tracer.Trace(ex.Message.ToString());
            }
        }
    }
}
