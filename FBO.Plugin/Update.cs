﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace F_B_O
{
   public class Update : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));
            if (context.Depth <= 2)
            {
                return;
            }
            try
            {
            tracer.Trace("Into FBO plugin to call api schedule......");
                if (context.InputParameters.Contains("Target") &&
                                    context.InputParameters["Target"] is Entity)
                {
                    var targetEntity1 = (Entity)context.InputParameters["Target"];
                    if (targetEntity1.LogicalName != "fbo_updates")
                        return;

                    if (targetEntity1.Attributes.Contains("fbo_opportunitylookup"))
                    {
                        tracer.Trace("have look up schedule");

                        var prreff = targetEntity1.GetAttributeValue<EntityReference>("fbo_opportunitylookup");

                        Entity opportunityRecord = service.Retrieve("opportunity", prreff.Id, new ColumnSet(new string[] 
                        {
                            "fbo_fbo_solicitation_number",
                            "fbo_fbo_agency",
                            "fbo_fbo_title",
                            "fbo_fbo_data_source",
                            "fbo_fbo_description",
                            "fbo_fbo_contact",
                            "fbo_fbo_naics_text",
                            "fbo_fbo_archive_date",
                            "fbo_fbo_class_code",
                            "fbo_fbo_office_address",
                            "fbo_fbo_pop_address",
                            "fbo_fbo_pop_country",
                            "fbo_fbo_pop_zip_code",
                            "fbo_fbo_setaside",
                            "fbo_fbo_posted_date",
                            "fbo_fbo_notice_type",
                            "fbo_fbo_id",
                            "fbo_fbo_office",
                            "fbo_fbo_location",
                            "fbo_fbo_zip_code",
                            "fbo_fbo_listing_url",

                        })); //AppSource
                       // Entity opportunityRecord = (Entity)context.InputParameters["Target"];
                        string SolNo = string.Empty;
                        tracer.Trace(" look up get schedule");

                        if (opportunityRecord.Attributes.Contains("fbo_fbo_solicitation_number"))
                        {
                            SolNo = opportunityRecord.Attributes["fbo_fbo_solicitation_number"].ToString();
                            tracer.Trace(" SolNo:- " + SolNo);
                        }

                        string solicitationNumber = "";
                        string apiKey = "";

                        // Get Solicitation Number from the current context
                        if (opportunityRecord.Contains("fbo_fbo_solicitation_number") && !string.IsNullOrEmpty(opportunityRecord.GetAttributeValue<string>("fbo_fbo_solicitation_number")))
                        {
                            solicitationNumber = opportunityRecord.GetAttributeValue<string>("fbo_fbo_solicitation_number");
                        }

                        // Get API Key from the custom Config Record
                        Entity fboConfig = getFBOConnectorConfigRecord(service,tracer);

                        if (fboConfig == null)
                        {
                            return;
                        }

                        apiKey = fboConfig.GetAttributeValue<string>("fbo_api_key");

                        tracer.Trace("2.Getting FBO Details for Sol No: " + solicitationNumber + " and API Key :" + apiKey);

                        if (!string.IsNullOrEmpty(solicitationNumber) && !string.IsNullOrEmpty(apiKey))
                        {
                            string url = "https://fbodata.herokuapp.com/opportunities/getFboOpp/" + solicitationNumber + "/" + apiKey;

                            //Entity targetEntity = (Entity)context.InputParameters["Target"];
                            Entity targetEntity = opportunityRecord;
                            tracer.Trace("3.Preparing Request Object for FBO Details");
                            EntityReference er = new EntityReference("opportunity", opportunityRecord.Id);
                            //EntityReference opportunityRef = new EntityReference("opportunity", opportunityRecord.Id);
                            EntityReference opportunityRef = er;

                            //List<FBOTimeLine> FboEntities1 = getFBORecords_Service_Test(url, fboConfig, tracer);

                            //tracer.Trace("FboEntities1 count :" + FboEntities1.Count);
                            tracer.Trace("3.calling getFBORecords_Service");

                            List<FBOTimeLine> FboEntities = getFBORecords_Service(url, fboConfig, tracer);

                            if (FboEntities != null && FboEntities.Count > 0)
                            {
                                tracer.Trace("7.Total Array elements : " + FboEntities.Count.ToString());

                                // Delete any existing FBO Attachment Records for this opportunity
                                deleteExistingFBOAttachments(opportunityRef, service,tracer);

                                foreach (FBOTimeLine fboRecord in FboEntities)
                                {
                                    if (string.IsNullOrEmpty(fboRecord.attach_url))
                                    {
                                        tracer.Trace("8.Creating FBO Timeline Record : ");

                                        // Check if there is any existing FBO TimeLine record for the Opportunity
                                        Entity existingFboTL = checkIsFBOTimeLineExisting(opportunityRef, service, tracer);

                                        createUpdateFBOTimeLine(context,fboRecord, opportunityRef, existingFboTL, service, tracer);

                                        //setFBODetailsonOpportunity(context, service, fboRecord, targetEntity, targetEntity, tracer);
                                        setFBODetailsonOpportunity(context, service, fboRecord, targetEntity, targetEntity, tracer);

                                    }
                                    else if (!string.IsNullOrEmpty(fboRecord.attach_url))
                                    {
                                        tracer.Trace("8.3.Creating FBO Attachment Records");
                                        createFBOAttachment(fboRecord, opportunityRef, service);
                                    }
                                }
                            }
                            else
                            {
                                createNotificationTracker(solicitationNumber, service, tracer);
                                tracer.Trace("No Data found in De-Serialization");
                                ClearFBODetailsonOpportunity(context, service, targetEntity, tracer);
                                //throw new InvalidPluginExecutionException("No Data found in De-Serialization");
                            }
                        }
                        else
                        {
                            tracer.Trace("Solicitation Number or API Key not found , please specify the values to get FBO Details");
                            Entity targetEntity = (Entity)context.InputParameters["Target"];
                            ClearFBODetailsonOpportunity(context, service, targetEntity, tracer);
                            // throw new InvalidPluginExecutionException("Solicitation Number or API Key not found , please specify the values to get FBO Details");
                        }
                    }





                }
                //TODO: Do stuff
            }
            catch (Exception e)
            {
                tracer.Trace(e.Message.ToString());
                //throw new InvalidPluginExecutionException(e.Message);
            }
        }

        private List<FBOTimeLine> getFBORecords_Service(string url, Entity fboConnector, ITracingService tracer)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.KeepAlive = false;
            request.Method = "GET";
            List<FBOTimeLine> FboEntities = new List<FBOTimeLine>();

            try
            {
                String username = fboConnector.GetAttributeValue<string>("fbo_username"); // "7VQWKzGg";
                String password = fboConnector.GetAttributeValue<string>("fbo_password"); //"7VQaLzWFXATvVBU=";
                tracer.Trace("Username = " + username);
                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                {
                    //throw new InvalidPluginExecutionException("There is no Username/Password found for FBO Connector Configuration");
                    tracer.Trace("There is no Username/Password found for FBO Connector Configuration");
                }
                else
                {
                    String encoded = Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                    request.Headers.Add("username", username);
                    request.Headers.Add("password", password);

                    WebResponse response = request.GetResponse();

                    tracer.Trace("4.Processing the Response");
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        tracer.Trace("5.Loaded the respose to Stream Reader");

                        string result = string.Empty;

                        if (!streamReader.EndOfStream)
                        {
                            tracer.Trace("5.1.Checking Stream Reader");
                            result = streamReader.ReadToEnd();
                        }

                        if (!string.IsNullOrEmpty(result))
                        {
                            var json = JsonConvert.DeserializeObject<RootObjectEntities>(result);
                            if (json != null && json.docs != null && json.docs.Count > 0)
                            {
                                tracer.Trace("6.Deserialized OK :" + json.docs[0].DataSource);
                                int i = 0;
                                foreach (var xy in json.docs)
                                {
                                    try
                                    {
                                        tracer.Trace("Test :" + i + json.docs[i].attach_title);
                                        i++;
                                    }
                                    catch (Exception ex)
                                    {
                                        //throw new InvalidPluginExecutionException("Error :" + ex.Message);
                                        tracer.Trace("Error:- " + ex.Message.ToString());
                                    }
                                }
                                FboEntities = json.docs;
                            }
                            else
                            {
                                tracer.Trace("6.1.Deserialization Failed :" + json);
                                //throw new InvalidPluginExecutionException("No Data returned from the webservice");
                            }
                        }
                    }
                    response.Dispose();//AppSource
                }
            }
            catch (Exception wex)
            {
                //throw new InvalidPluginExecutionException("Error occured while retrieving FBO Details :" + wex.Message.ToString());
                tracer.Trace("Error occured while retrieving FBO Details :" + wex.Message.ToString());
            }

            return FboEntities;
        }
        private void ClearFBODetailsonOpportunity(IPluginExecutionContext context, IOrganizationService service, Entity opportunity, ITracingService tracer)
        {
            try
            {
                tracer.Trace("Clearing FBO Details on Opportunity Entity ");
                opportunity["fbo_fbo_agency"] = "";
                opportunity["fbo_fbo_title"] = "";
                opportunity["fbo_fbo_data_source"] = "";
                opportunity["fbo_fbo_description"] = "";
                opportunity["fbo_fbo_contact"] = "";
                opportunity["fbo_fbo_naics_text"] = "";
                opportunity["fbo_fbo_archive_date"] = null;
                opportunity["fbo_fbo_class_code"] = "";
                opportunity["fbo_fbo_office_address"] = "";
                opportunity["fbo_fbo_pop_address"] = "";
                opportunity["fbo_fbo_pop_country"] = "";
                opportunity["fbo_fbo_pop_zip_code"] = "";
                opportunity["fbo_fbo_setaside"] = "";
                opportunity["fbo_fbo_posted_date"] = null;
                opportunity["fbo_fbo_notice_type"] = "";
                opportunity["fbo_fbo_id"] = "";
                opportunity["fbo_fbo_office"] = "";
                opportunity["fbo_fbo_location"] = "";
                opportunity["fbo_fbo_zip_code"] = "";
                opportunity["fbo_fbo_listing_url"] = "";
                //context.InputParameters["Target"] = opportunity;
                service.Update(opportunity);
                tracer.Trace("Trace:- FBO Details has been cleared.");

            }
            catch (Exception ex)
            {
                tracer.Trace("Error While clearing FBO Details on Opportunity Entity :-" + ex.Message.ToString());
            }
        }
        private void createUpdateFBOTimeLine(IPluginExecutionContext context,FBOTimeLine FboEntity, EntityReference opp, Entity existingFboTL, IOrganizationService service, ITracingService tracer)
        {
            Entity record = new Entity("fbo_fbotimeline");

            tracer.Trace("Inside createUpdateFBOTimeLine");

            try
            {

                // Need to update FBO TimeLine if there is FBO TL exists. Else need to create new FBO TL
                if (existingFboTL != null && existingFboTL.Id != null && existingFboTL.Id != Guid.Empty)
                {
                    record.Id = existingFboTL.Id;
                }


                if (opp.Id != null && opp.Id != Guid.Empty)
                {
                    record["fbo_opportunity"] = opp;
                }

                if (!string.IsNullOrEmpty(FboEntity.SolNbr))
                {
                    record["fbo_solicitation_number"] = FboEntity.SolNbr;
                }
                else
                {
                    record["fbo_solicitation_number"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.Agency))
                {
                    record["fbo_agency"] = FboEntity.Agency;
                }
                else
                {
                    record["fbo_agency"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.Title))
                {
                    record["fbo_title"] = FboEntity.Title;
                }
                else
                {
                    record["fbo_title"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.Title))
                {
                    record["fbo_name"] = FboEntity.Title;
                }
                else
                {
                    record["fbo_name"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.DataSource))
                {
                    record["fbo_data_source"] = FboEntity.DataSource;
                }
                else
                {
                    record["fbo_data_source"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.Description))
                {
                    record["fbo_description"] = FboEntity.Description;
                }
                else
                {
                    record["fbo_description"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.FBOcontact))
                {
                    record["fbo_contact"] = FboEntity.FBOcontact;
                }
                else
                {
                    record["fbo_contact"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.FBONAICS))
                {
                    record["fbo_naics_text"] = FboEntity.FBONAICS;
                }
                else
                {
                    record["fbo_naics_text"] = "";
                }

                if (FboEntity.FBO_archivedate != null)
                {
                    record["fbo_archive_date"] = FboEntity.FBO_archivedate;
                }
                else
                {
                    record["fbo_archive_date"] = null;
                }


                if (!string.IsNullOrEmpty(FboEntity.FBO_classcode))
                {
                    record["fbo_class_code"] = FboEntity.FBO_classcode;
                }
                else
                {
                    record["fbo_class_code"] = "";
                }


                if (!string.IsNullOrEmpty(FboEntity.FBO_officeaddress))
                {
                    record["fbo_office_address"] = FboEntity.FBO_officeaddress;
                }
                else
                {
                    record["fbo_office_address"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.FBO_PopAddress))
                {
                    record["fbo_pop_address"] = FboEntity.FBO_PopAddress;
                }
                else
                {
                    record["fbo_pop_address"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.FBO_PopCountry))
                {
                    record["fbo_pop_country"] = FboEntity.FBO_PopCountry;
                }
                else
                {
                    record["fbo_pop_country"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.FBO_PopZipCode))
                {
                    record["fbo_pop_zip_code"] = FboEntity.FBO_PopZipCode;
                }
                else
                {
                    record["fbo_pop_zip_code"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.FBO_Setaside))
                {
                    record["fbo_setaside"] = FboEntity.FBO_Setaside;
                }
                else
                {
                    record["fbo_setaside"] = "";
                }

                if (FboEntity.Postdate != null)
                {
                    record["fbo_posted_date"] = FboEntity.Postdate;
                }
                else
                {
                    record["fbo_posted_date"] = null;
                }

                if (!string.IsNullOrEmpty(FboEntity.NoticeType))
                {
                    record["fbo_notice_type"] = FboEntity.NoticeType;
                }
                else
                {
                    record["fbo_notice_type"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.ID))
                {
                    record["fbo_id"] = FboEntity.ID;
                }
                else
                {
                    record["fbo_id"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.Office))
                {
                    record["fbo_office"] = FboEntity.Office;
                }
                else
                {
                    record["fbo_office"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.Location))
                {
                    record["fbo_location"] = FboEntity.Location;
                }
                else
                {
                    record["fbo_location"] = "";
                }

                if (!string.IsNullOrEmpty(FboEntity.Zipcode))
                {
                    record["fbo_zip_code"] = FboEntity.Zipcode;
                }
                else
                {
                    record["fbo_zip_code"] = "";
                }
                if (!string.IsNullOrEmpty(FboEntity.Listing_url))
                {
                    record["fbo_listing_url"] = FboEntity.Listing_url;
                }
                else
                {
                    record["fbo_listing_url"] = "";
                }

                // Update if Existing FBO TL is not null else Create new FBO TL.
                if (existingFboTL != null && existingFboTL.Id != null && existingFboTL.Id != Guid.Empty)
                {
                    tracer.Trace("Updating Existing TimeLine Record :" + existingFboTL.Id);
                    //context.InputParameters["Target"] = record;
                    service.Update(record);
                }
                else
                {
                    tracer.Trace("New TimeLine record is Creating");
                    service.Create(record);
                }
            }
            catch (Exception ex)
            {
                tracer.Trace("Error Occured :" + ex.Message);
                //throw new InvalidPluginExecutionException("Error Occred :" + ex.Message);
            }
        }

        private void createFBOAttachment(FBOTimeLine FboEntity, EntityReference opp, IOrganizationService service)
        {
            Entity fboAttachmentrecord = new Entity("fbo_fbo_attachments");

            if (opp.Id != null && opp.Id != Guid.Empty)
            {
                fboAttachmentrecord["fbo_opportunity"] = opp;
            }

            if (!string.IsNullOrEmpty(FboEntity.attach_title))
            {
                fboAttachmentrecord["fbo_attachment_title"] = FboEntity.attach_title;
            }

            if (!string.IsNullOrEmpty(FboEntity.attach_url))
            {
                fboAttachmentrecord["fbo_attachment_url"] = FboEntity.attach_url;
            }

            if (!string.IsNullOrEmpty(FboEntity.SolNbr))
            {
                fboAttachmentrecord["fbo_solicitation_number"] = FboEntity.SolNbr;
            }

            service.Create(fboAttachmentrecord);
        }

        private void setFBODetailsonOpportunity(IPluginExecutionContext context, IOrganizationService service, FBOTimeLine FboEntity, Entity opportunity, Entity preOpportunity, ITracingService tracer)
        {
            tracer.Trace("9.Setting FBO Details on Opportunity Entity ");
            //tracer.Trace("opportunity.Attributes.Count:- " + opportunity.Attributes.Count);
            //opportunity = service.Retrieve("opportunity", opportunityRef.Id,new ColumnSet(true));
            tracer.Trace("opportunity.Attributes.Count:- " + opportunity.Attributes.Count);
            try
            {
                if (preOpportunity.GetAttributeValue<string>("fbo_fbo_agency") != FboEntity.Agency)
                {
                    if (!string.IsNullOrEmpty(FboEntity.Agency))
                    {
                        opportunity["fbo_fbo_agency"] = FboEntity.Agency;
                        tracer.Trace("9.1  fbo_fbo_agency updated.");

                    }
                    else
                    {
                        opportunity["fbo_fbo_agency"] = "";
                    }
                }
                else
                {
                    tracer.Trace("9.1  fbo_fbo_agency not updated." + FboEntity.Agency);
                    tracer.Trace("9.1  fbo_fbo_agency not updated." + preOpportunity.GetAttributeValue<string>("fbo_fbo_agency"));

                }
            }
            catch (Exception ex)
            {
                tracer.Trace("9.1  fbo_fbo_agency not updated." + ex.Message.ToString());
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_title") != FboEntity.Title)
            {
                if (!string.IsNullOrEmpty(FboEntity.Title))
                {
                    opportunity["fbo_fbo_title"] = FboEntity.Title;
                }
                else
                {
                    opportunity["fbo_fbo_title"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_data_source") != FboEntity.DataSource)
            {
                if (!string.IsNullOrEmpty(FboEntity.DataSource))
                {
                    opportunity["fbo_fbo_data_source"] = FboEntity.DataSource;
                }
                else
                {
                    opportunity["fbo_fbo_data_source"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_description") != FboEntity.Description.Replace("&nbsp;", " "))
            {
                if (!string.IsNullOrEmpty(FboEntity.Description))
                {
                    opportunity["fbo_fbo_description"] = FboEntity.Description.Replace("&nbsp;"," ");
                }
                else
                {
                    opportunity["fbo_fbo_description"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_contact") != FboEntity.FBOcontact)
            {
                if (!string.IsNullOrEmpty(FboEntity.FBOcontact))
                {
                    opportunity["fbo_fbo_contact"] = FboEntity.FBOcontact;
                }
                else
                {
                    opportunity["fbo_fbo_contact"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_naics_text") != FboEntity.FBONAICS)
            {
                if (!string.IsNullOrEmpty(FboEntity.FBONAICS))
                {
                    opportunity["fbo_fbo_naics_text"] = FboEntity.FBONAICS;
                }
                else
                {
                    opportunity["fbo_fbo_naics_text"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<DateTime>("fbo_fbo_archive_date") != FboEntity.FBO_archivedate)
            {
                if (FboEntity.FBO_archivedate != null)
                {
                    opportunity["fbo_fbo_archive_date"] = FboEntity.FBO_archivedate;
                }
                else
                {
                    opportunity["fbo_fbo_archive_date"] = null;
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_class_code") != FboEntity.FBO_classcode)
            {
                if (!string.IsNullOrEmpty(FboEntity.FBO_classcode))
                {
                    opportunity["fbo_fbo_class_code"] = FboEntity.FBO_classcode;
                }
                else
                {
                    opportunity["fbo_fbo_class_code"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_office_address") != FboEntity.FBO_officeaddress)
            {
                if (!string.IsNullOrEmpty(FboEntity.FBO_officeaddress))
                {
                    opportunity["fbo_fbo_office_address"] = FboEntity.FBO_officeaddress;
                }
                else
                {
                    opportunity["fbo_fbo_office_address"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_pop_address") != FboEntity.FBO_PopAddress)
            {
                if (!string.IsNullOrEmpty(FboEntity.FBO_PopAddress))
                {
                    opportunity["fbo_fbo_pop_address"] = FboEntity.FBO_PopAddress;
                }
                else
                {
                    opportunity["fbo_fbo_pop_address"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_pop_country") != FboEntity.FBO_PopCountry)
            {
                if (!string.IsNullOrEmpty(FboEntity.FBO_PopCountry))
                {
                    opportunity["fbo_fbo_pop_country"] = FboEntity.FBO_PopCountry;
                }
                else
                {
                    opportunity["fbo_fbo_pop_country"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_pop_zip_code") != FboEntity.FBO_PopZipCode)
            {
                if (!string.IsNullOrEmpty(FboEntity.FBO_PopZipCode))
                {
                    opportunity["fbo_fbo_pop_zip_code"] = FboEntity.FBO_PopZipCode;
                }
                else
                {
                    opportunity["fbo_fbo_pop_zip_code"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_setaside") != FboEntity.FBO_Setaside)
            {
                if (!string.IsNullOrEmpty(FboEntity.FBO_Setaside))
                {
                    opportunity["fbo_fbo_setaside"] = FboEntity.FBO_Setaside;
                }
                else
                {
                    opportunity["fbo_fbo_setaside"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<DateTime>("fbo_fbo_posted_date") != FboEntity.Postdate)
            {
                if (FboEntity.Postdate != null)
                {
                    opportunity["fbo_fbo_posted_date"] = FboEntity.Postdate;
                }
                else
                {
                    opportunity["fbo_fbo_posted_date"] = null;
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_notice_type") != FboEntity.NoticeType)
            {
                if (!string.IsNullOrEmpty(FboEntity.NoticeType))
                {
                    opportunity["fbo_fbo_notice_type"] = FboEntity.NoticeType;
                }
                else
                {
                    opportunity["fbo_fbo_notice_type"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_id") != FboEntity.ID)
            {
                if (!string.IsNullOrEmpty(FboEntity.ID))
                {
                    opportunity["fbo_fbo_id"] = FboEntity.ID;
                }
                else
                {
                    opportunity["fbo_fbo_id"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_office") != FboEntity.Office)
            {
                if (!string.IsNullOrEmpty(FboEntity.Office))
                {
                    opportunity["fbo_fbo_office"] = FboEntity.Office;
                }
                else
                {
                    opportunity["fbo_fbo_office"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_location") != FboEntity.Location)
            {
                if (!string.IsNullOrEmpty(FboEntity.Location))
                {
                    opportunity["fbo_fbo_location"] = FboEntity.Location;
                }
                else
                {
                    opportunity["fbo_fbo_location"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_zip_code") != FboEntity.Zipcode)
            {
                if (!string.IsNullOrEmpty(FboEntity.Zipcode))
                {
                    opportunity["fbo_fbo_zip_code"] = FboEntity.Zipcode;
                }
                else
                {
                    opportunity["fbo_fbo_zip_code"] = "";
                }
            }

            if (preOpportunity.GetAttributeValue<string>("fbo_fbo_listing_url") != FboEntity.Listing_url)
            {
                if (!string.IsNullOrEmpty(FboEntity.Listing_url))
                {
                    opportunity["fbo_fbo_listing_url"] = FboEntity.Listing_url;
                }
                else
                {
                    opportunity["fbo_fbo_listing_url"] = "";
                }
            }

            if (context.Stage == 40)
            {
                //opportunity["fbo_get_fbo_date"] = DateTime.Now;
                //context.InputParameters["Target"] = opportunity;
                service.Update(opportunity);
                tracer.Trace("Opportunity Updated");

            }
        }

        private Entity getFBOConnectorConfigRecord(IOrganizationService service,ITracingService tracer)
        {
            Entity fboConnector = null;

            try
            {
                QueryExpression query = new QueryExpression("fbo_fbo_connector");
                query.NoLock = true;
                query.ColumnSet = new ColumnSet(new string[] { "fbo_api_key", "fbo_username", "fbo_password" });
                query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));

                EntityCollection results = service.RetrieveMultiple(query);

                if (results != null && results.Entities.Count > 1)
                {
                    tracer.Trace("There are more than one FBO Connector Configuration found");
                    //throw new InvalidPluginExecutionException("There are more than one FBO Connector Configuration found");
                }
                else if (results == null)
                {
                    tracer.Trace("There is no FBO Connector Configuration found");
                    //throw new InvalidPluginExecutionException("There is no FBO Connector Configuration found");
                }
                else
                {
                    fboConnector = results.Entities[0];
                }
            }
            catch (Exception ex)
            {
                    tracer.Trace("Error occured while retrieving FBO Connector Configuration Record from CRM. " + ex.Message.ToString());
                //throw new InvalidPluginExecutionException("Error occured while retrieving FBO Connector Configuration Record from CRM. " + ex.Message.ToString());
            }

            return fboConnector;
        }

        private Entity checkIsFBOTimeLineExisting(EntityReference opp, IOrganizationService service, ITracingService tracer)
        {
            Entity existingFBOTimeLine = null;

            tracer.Trace("8.1 - Check Is FBO Time Line Existing ?");

            try
            {
                QueryExpression query = new QueryExpression("fbo_fbotimeline");
                query.NoLock = true;
                query.ColumnSet = new ColumnSet(new string[] { "fbo_title" });
                query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
                query.Criteria.AddCondition(new ConditionExpression("fbo_opportunity", ConditionOperator.Equal, opp.Id));

                EntityCollection results = service.RetrieveMultiple(query);

                if (results != null && results.Entities.Count > 0)
                {
                    tracer.Trace("8.2 - Found (" + results.Entities.Count + ")  Existing FBO Time Lines");
                    existingFBOTimeLine = results.Entities[0];
                }
            }
            catch (Exception ex)
            {
                tracer.Trace("Error occured while retrieving FBO Connector Configuration Record from CRM. " + ex.Message.ToString());
                //throw new InvalidPluginExecutionException("Error occured while retrieving FBO Connector Configuration Record from CRM. " + ex.Message.ToString());
            }

            return existingFBOTimeLine;
        }

        private void deleteExistingFBOAttachments(EntityReference opp, IOrganizationService service, ITracingService tracer)
        {
            try
            {
                QueryExpression query = new QueryExpression("fbo_fbo_attachments");
                query.NoLock = true;
                query.ColumnSet = new ColumnSet(new string[] { "fbo_attachment_title" });
                query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
                query.Criteria.AddCondition(new ConditionExpression("fbo_opportunity", ConditionOperator.Equal, opp.Id));

                EntityCollection results = service.RetrieveMultiple(query);

                if (results != null && results.Entities.Count > 0)
                {
                    foreach (Entity fboAttachment in results.Entities)
                    {
                        service.Delete("fbo_fbo_attachments", fboAttachment.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                tracer.Trace("Error occured while retrieving FBO Connector Configuration Record from CRM. " + ex.Message.ToString());
                //throw new InvalidPluginExecutionException("Error occured while retrieving FBO Connector Configuration Record from CRM. " + ex.Message.ToString());
            }
        }

        private void createNotificationTracker(string solicitationNumber, IOrganizationService service, ITracingService tracer)
        {
            tracer.Trace("10.1 - Inside createNotificationTracker");

            try
            {
                Entity notificationTracker = new Entity("fbo_fbonotificationtracker");
                notificationTracker["fbo_name"] = "Issue - Solicitation Not Found";
                notificationTracker["fbo_solicitation_number"] = solicitationNumber;

                service.Create(notificationTracker);
            }
            catch (Exception ex)
            {
                //throw new InvalidPluginExecutionException("Error occured while creating Notification Tracker record in CRM. " + ex.Message.ToString());
                tracer.Trace("Error occured while creating Notification Tracker record in CRM. " + ex.Message.ToString());

            }
        }

        public class FBOTimeLine
        {
            public string DataSource { get; set; }
            public string NoticeType { get; set; }
            public string SolNbr { get; set; }
            public string ID { get; set; }
            public DateTime? Postdate { get; set; }
            public string Agency { get; set; }
            public string Office { get; set; }
            public string Location { get; set; }
            public string Zipcode { get; set; }
            public string FBO_classcode { get; set; }
            public string FBONAICS { get; set; }
            public string FBO_officeaddress { get; set; }
            public string Title { get; set; }
            public DateTime? FBO_archivedate { get; set; }
            public string FBOcontact { get; set; }
            public string Description { get; set; }
            public string Listing_url { get; set; }
            public string FBO_Setaside { get; set; }
            public string FBO_PopCountry { get; set; }
            public string FBO_PopZipCode { get; set; }
            public string FBO_PopAddress { get; set; }
            public string attach_title { get; set; }
            public string attach_url { get; set; }
        }

        public class RootObjectEntities
        {
            public List<FBOTimeLine> docs { get; set; }
        }
    }
}
